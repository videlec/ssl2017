# "Probability and dynamical systems", Luminy, 2017

All the links below point to ipynb files (in json format). To use it, you need to save
them on your computers in a place where the Jupyter notebook can access them.

## Tuesday 4th

### morning: programming

- lists [chap12-list.en.ipynb](https://framagit.org/vdelecroix/ssl2017/raw/master/programming/chap12-list.en.ipynb)

- for loops [chap13-for.en.ipynb](https://framagit.org/vdelecroix/ssl2017/raw/master/programming/chap13-for.en.ipynb)

- if statements [chap14-if.en.ipynb](https://framagit.org/vdelecroix/ssl2017/raw/master/programming/chap14-if.en.ipynb)

- functions [chap15-def.en.ipynb](https://framagit.org/vdelecroix/ssl2017/raw/master/programming/chap15-def.en.ipynb)

- while loops [chap16-while.en.ipynb](https://framagit.org/vdelecroix/ssl2017/raw/master/programming/chap16-while.en.ipynb)

- exercises [chap17-prog.en.ipynb](https://framagit.org/vdelecroix/ssl2017/raw/master/programming/chap17-prog.en.ipynb)

### afternoon: math exploration

Dynamical systems:

- exact and approximate computations / stability and unstability [exact_versus_floating_point.ipynb](https://framagit.org/vdelecroix/ssl2017/raw/master/exact_versus_floating_point/exact_versus_floating_point.ipynb)

- matrix dynamics [matrix_dynamics.ipynb](https://framagit.org/vdelecroix/ssl2017/raw/master/matrix_dynamics/matrix_dynamics.ipynb)

Probabilities:

- random walks [random_walk.ipynb](https://framagit.org/vdelecroix/ssl2017/raw/master/random_walk/random_walk.ipynb)

- markov chains [markov.ipynb](https://framagit.org/vdelecroix/ssl2017/raw/master/markov/markov.ipynb)


